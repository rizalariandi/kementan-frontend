import { Route } from '@angular/router';
import { LockComponent } from './lock.component';

export const LockRoutes: Route[] = [
    {
      path: 'lock',
      component: LockComponent
    }
];
