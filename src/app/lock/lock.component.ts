import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded SignupComponent.
*/

@Component({
  selector: 'app-signup-cmp',
  templateUrl: 'lock.component.html'
})

export class LockComponent { }
