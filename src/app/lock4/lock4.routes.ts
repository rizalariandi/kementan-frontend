import { Route } from '@angular/router';
import { Lock4Component } from './lock4.component';

export const Lock4Routes: Route[] = [
    {
      path: 'lock4',
      component: Lock4Component
    }
];
