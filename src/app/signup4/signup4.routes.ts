import { Route } from '@angular/router';
import { Signup4Component } from './signup4.component';

export const Signup4Routes: Route[] = [
    {
      path: 'signup4',
      component: Signup4Component
    }
];
