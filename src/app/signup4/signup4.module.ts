import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Signup4Component } from './signup4.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Signup4Component],
    exports: [Signup4Component]
})

export class Signup4Module { }
