import { Route } from '@angular/router';
import { Resetpassword5Component } from './resetpassword5.component';

export const Resetpassword5Routes: Route[] = [
    {
      path: 'resetpassword6',
      component: Resetpassword5Component
    }
];
