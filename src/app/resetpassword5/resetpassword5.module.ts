import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Resetpassword5Component } from './resetpassword5.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Resetpassword5Component],
    exports: [Resetpassword5Component]
})

export class Resetpassword5Module { }
