import { Route } from '@angular/router';
import { ResetpasswordComponent } from './resetpassword.component';

export const ResetpasswordRoutes: Route[] = [
    {
      path: 'resetpassword',
      component: ResetpasswordComponent
    }
];
