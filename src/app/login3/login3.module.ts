import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Login3Component } from './login3.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Login3Component],
    exports: [Login3Component]
})

export class Login3Module { }
