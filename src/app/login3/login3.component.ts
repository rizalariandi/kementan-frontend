import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-login-cmp',
  templateUrl: 'login3.component.html'
})

export class Login3Component { }
