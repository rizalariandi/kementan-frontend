import { Routes } from '@angular/router';
import { Login3Component } from './login3.component';

export const Login3Routes: Routes = [
    {
      path: 'signin3', component: Login3Component
    }
];
