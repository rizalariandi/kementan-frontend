import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Signup5Component } from './signup5.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Signup5Component],
    exports: [Signup5Component]
})

export class Signup5Module { }
