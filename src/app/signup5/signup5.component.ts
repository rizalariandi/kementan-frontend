import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded SignupComponent.
*/

@Component({
  selector: 'app-signup-cmp',
  templateUrl: 'signup5.component.html'
})

export class Signup5Component { }
