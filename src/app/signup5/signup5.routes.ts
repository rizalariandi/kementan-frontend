import { Route } from '@angular/router';
import { Signup5Component } from './signup5.component';

export const Signup5Routes: Route[] = [
    {
      path: 'signup5',
      component: Signup5Component
    }
];
