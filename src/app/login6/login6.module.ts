import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Login6Component } from './login6.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Login6Component],
    exports: [Login6Component]
})

export class Login6Module { }
