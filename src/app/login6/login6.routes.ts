import { Routes } from '@angular/router';
import { Login6Component } from './login6.component';

export const Login6Routes: Routes = [
    {
      path: 'signin6', component: Login6Component
    }
];
