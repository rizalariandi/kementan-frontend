import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Login4Component } from './login4.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Login4Component],
    exports: [Login4Component]
})

export class Login4Module { }
