import { Routes } from '@angular/router';
import { Login4Component } from './login4.component';

export const Login4Routes: Routes = [
    {
      path: 'signin4', component: Login4Component
    }
];
