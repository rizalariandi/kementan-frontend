import { Routes } from '@angular/router';
import { Login2Component } from './login2.component';

export const Login2Routes: Routes = [
    {
      path: 'signin2', component: Login2Component
    }
];
