import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Resetpassword6Component } from './resetpassword6.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Resetpassword6Component],
    exports: [Resetpassword6Component]
})

export class Resetpassword6Module { }
