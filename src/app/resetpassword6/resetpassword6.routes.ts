import { Route } from '@angular/router';
import { Resetpassword6Component } from './resetpassword6.component';

export const Resetpassword6Routes: Route[] = [
    {
      path: 'resetpassword6',
      component: Resetpassword6Component
    }
];
