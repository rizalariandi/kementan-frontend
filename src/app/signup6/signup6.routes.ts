import { Route } from '@angular/router';
import { Signup6Component } from './signup6.component';

export const Signup6Routes: Route[] = [
    {
      path: 'signup6',
      component: Signup6Component
    }
];
