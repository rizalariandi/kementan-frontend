import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Signup6Component } from './signup6.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Signup6Component],
    exports: [Signup6Component]
})

export class Signup6Module { }
