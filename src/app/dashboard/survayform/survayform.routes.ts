import { Route } from '@angular/router';

import { SurvayformComponent } from './index';

export const SurvayformRoutes: Route[] = [
  {
    path: 'survayform',
    component: SurvayformComponent
  }
];
