import { NgModule } from '@angular/core';

import { SurvayformComponent } from './survayform.component';

@NgModule({
    imports: [],
    declarations: [SurvayformComponent],
    exports: [SurvayformComponent]
})

export class SurvayformModule { }
