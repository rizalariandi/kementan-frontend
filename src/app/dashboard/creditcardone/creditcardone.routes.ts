import { Route } from '@angular/router';

import { CreditcardoneComponent } from './index';

export const CreditcardoneRoutes: Route[] = [
  {
    path: 'creditcardone',
    component: CreditcardoneComponent
  }
];
