import { NgModule } from '@angular/core';

import { CreditcardoneComponent } from './creditcardone.component';

@NgModule({
    imports: [],
    declarations: [CreditcardoneComponent],
    exports: [CreditcardoneComponent]
})

export class CreditcardoneModule { }
