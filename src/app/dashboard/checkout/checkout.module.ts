import { NgModule } from '@angular/core';

import { CheckoutComponent } from './checkout.component';

@NgModule({
    imports: [],
    declarations: [CheckoutComponent],
    exports: [CheckoutComponent]
})

export class CheckoutModule { }
