import { Route } from '@angular/router';

import { CheckoutComponent } from './index';

export const CheckoutRoutes: Route[] = [
  {
    path: 'checkout',
    component: CheckoutComponent
  }
];
