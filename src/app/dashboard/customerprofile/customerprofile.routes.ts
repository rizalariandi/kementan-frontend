import { Route } from '@angular/router';

import { CustomerprofileComponent } from './index';

export const CustomerprofileRoutes: Route[] = [
  {
    path: 'customerprofile',
    component: CustomerprofileComponent
  }
];
