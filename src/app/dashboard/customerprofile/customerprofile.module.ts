import { NgModule } from '@angular/core';

import { CustomerprofileComponent } from './customerprofile.component';

@NgModule({
    imports: [],
    declarations: [CustomerprofileComponent],
    exports: [CustomerprofileComponent]
})

export class CustomerprofileModule { }
