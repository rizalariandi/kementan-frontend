import { Route } from '@angular/router';

import { TourintroComponent } from './index';

export const TourintroRoutes: Route[] = [
  {
    path: 'tourintro',
    component: TourintroComponent
  }
];
