import { NgModule } from '@angular/core';

import { TourintroComponent } from './tourintro.component';

@NgModule({
    imports: [],
    declarations: [TourintroComponent],
    exports: [TourintroComponent]
})

export class TourintroModule { }
