import { Route } from '@angular/router';

import { ForumtopicdetailsComponent } from './index';

export const ForumtopicdetailsRoutes: Route[] = [
  {
    path: 'forumtopicsdetails',
    component: ForumtopicdetailsComponent
  }
];
