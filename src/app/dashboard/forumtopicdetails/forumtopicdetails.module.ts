import { NgModule } from '@angular/core';

import { ForumtopicdetailsComponent } from './forumtopicdetails.component';

@NgModule({
    imports: [],
    declarations: [ForumtopicdetailsComponent],
    exports: [ForumtopicdetailsComponent]
})

export class ForumtopicdetailsModule { }
