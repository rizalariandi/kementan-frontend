import { Route } from '@angular/router';

import { InvoicetwoComponent } from './index';

export const InvoicetwoRoutes: Route[] = [
  {
    path: 'invoice2',
    component: InvoicetwoComponent
  }
];
