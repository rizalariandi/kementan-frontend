import { NgModule } from '@angular/core';

import { InvoicetwoComponent } from './invoicetwo.component';

@NgModule({
    imports: [],
    declarations: [InvoicetwoComponent],
    exports: [InvoicetwoComponent]
})

export class InvoicetwoModule { }
