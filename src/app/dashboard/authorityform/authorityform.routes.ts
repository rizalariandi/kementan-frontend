import { Route } from '@angular/router';

import { AuthorityformComponent } from './index';

export const AuthorityformRoutes: Route[] = [
  {
    path: 'authorityform',
    component: AuthorityformComponent
  }
];
