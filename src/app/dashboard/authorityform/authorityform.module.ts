import { NgModule } from '@angular/core';

import { AuthorityformComponent } from './authorityform.component';

@NgModule({
    imports: [],
    declarations: [AuthorityformComponent],
    exports: [AuthorityformComponent]
})

export class AuthorityformModule { }
