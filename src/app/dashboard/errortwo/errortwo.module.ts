import { NgModule } from '@angular/core';

import { ErrortwoComponent } from './errortwo.component';

@NgModule({
    imports: [],
    declarations: [ErrortwoComponent],
    exports: [ErrortwoComponent]
})

export class ErrortwoModule { }
