import { Route } from '@angular/router';

import { ErrortwoComponent } from './index';

export const ErrortwoRoutes: Route[] = [
  {
    path: 'errortwo',
    component: ErrortwoComponent
  }
];
