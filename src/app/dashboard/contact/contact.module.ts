import { NgModule } from '@angular/core';

import { ContactComponent } from './contact.component';

@NgModule({
    imports: [],
    declarations: [ContactComponent],
    exports: [ContactComponent]
})

export class ContactModule { }
