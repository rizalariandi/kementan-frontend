import { Route } from '@angular/router';
import { MoreroundedComponent } from './index';

export const MoreroundedRoutes: Route[] = [
    {
      path: 'morerounded',
      component: MoreroundedComponent
    }
];
