import { Route } from '@angular/router';

import { PickerComponent } from './index';

export const PickerRoutes: Route[] = [
  {
    path: 'Picker',
    component: PickerComponent
  }
];
