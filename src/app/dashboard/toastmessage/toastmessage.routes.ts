import { Route } from '@angular/router';

import { ToastmessageComponent } from './index';

export const ToastmessageRoutes: Route[] = [
  {
    path: 'toastmessage',
    component: ToastmessageComponent
  }
];
