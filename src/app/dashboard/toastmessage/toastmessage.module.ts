import { NgModule } from '@angular/core';

import { ToastmessageComponent } from './toastmessage.component';

@NgModule({
    imports: [],
    declarations: [ToastmessageComponent],
    exports: [ToastmessageComponent]
})

export class ToastmessageModule { }
