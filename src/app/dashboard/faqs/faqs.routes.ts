import { Route } from '@angular/router';

import { FaqsComponent } from './index';

export const FaqsRoutes: Route[] = [
  {
    path: 'faqs',
    component: FaqsComponent
  }
];
