import { NgModule } from '@angular/core';

import { FaqsComponent } from './faqs.component';

@NgModule({
    imports: [],
    declarations: [FaqsComponent],
    exports: [FaqsComponent]
})

export class FaqsModule { }
