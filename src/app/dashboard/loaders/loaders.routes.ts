import { Route } from '@angular/router';

import { LoadersComponent } from './index';

export const LoadersRoutes: Route[] = [
  {
    path: 'loaders',
    component: LoadersComponent
  }
];
