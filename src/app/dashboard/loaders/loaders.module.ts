import { NgModule } from '@angular/core';

import { LoadersComponent } from './loaders.component';

@NgModule({
    imports: [],
    declarations: [LoadersComponent],
    exports: [LoadersComponent]
})

export class LoadersModule { }
