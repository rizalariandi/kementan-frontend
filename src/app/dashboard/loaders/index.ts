/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from './loaders.component';
export * from './loaders.routes';

