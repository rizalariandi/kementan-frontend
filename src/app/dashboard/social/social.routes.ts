import { Route } from '@angular/router';
import { SocialComponent } from './index';

export const SocialRoutes: Route[] = [
    {
      path: 'social',
      component: SocialComponent
    }
];
