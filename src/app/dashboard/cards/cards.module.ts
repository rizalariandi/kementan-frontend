import { NgModule } from '@angular/core';

import { CardsComponent } from './cards.component';

@NgModule({
    imports: [],
    declarations: [CardsComponent],
    exports: [CardsComponent]
})

export class CardsModule { }
