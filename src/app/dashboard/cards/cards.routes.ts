import { Route } from '@angular/router';

import { CardsComponent } from './index';

export const CardsRoutes: Route[] = [
  {
    path: 'cards',
    component: CardsComponent
  }
];
