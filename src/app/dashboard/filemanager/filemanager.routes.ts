import { Route } from '@angular/router';

import { FilemanagerComponent } from './index';

export const FilemanagerRoutes: Route[] = [
  {
    path: 'filemanager',
    component: FilemanagerComponent
  }
];
