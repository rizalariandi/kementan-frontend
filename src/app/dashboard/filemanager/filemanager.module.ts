import { NgModule } from '@angular/core';

import { FilemanagerComponent } from './filemanager.component';

@NgModule({
    imports: [],
    declarations: [FilemanagerComponent],
    exports: [FilemanagerComponent]
})

export class FilemanagerModule { }
