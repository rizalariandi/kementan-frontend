import { NgModule } from '@angular/core';

import { TabsComponent } from './tabs.component';

@NgModule({
    imports: [],
    declarations: [TabsComponent],
    exports: [TabsComponent]
})

export class TabsModules { }
