import { Route } from '@angular/router';

import { TabsComponent } from './index';

export const TabsRoutes: Route[] = [
  {
    path: 'tabs',
    component: TabsComponent
  }
];
