import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RtlComponent } from './rtl.component';
import { AccordionModule, AlertModule,
    ButtonsModule, CarouselModule, CollapseModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProgressbarModule,
    RatingModule,
    SortableModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';

import { TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent } from './rtl.component';

@NgModule({
    imports: [CommonModule, ChartsModule,
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        PopoverModule,
        ProgressbarModule,
        RatingModule,
        SortableModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule.forRoot()],
    declarations: [RtlComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent],
    exports: [RtlComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent]
})

export class RtlModule { }
