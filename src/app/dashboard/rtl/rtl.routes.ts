import { Route } from '@angular/router';
import { RtlComponent } from './index';

export const RtlRoutes: Route[] = [
    {
      path: 'rtl',
      component: RtlComponent
    }
];
