import { Route } from '@angular/router';

import { BreadcrumbComponent } from './index';

export const BreadcrumbRoutes: Route[] = [
  {
    path: 'breadcrumb',
    component: BreadcrumbComponent
  }
];
