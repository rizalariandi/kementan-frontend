import { NgModule } from '@angular/core';

import { BreadcrumbComponent } from './breadcrumb.component';

@NgModule({
    imports: [],
    declarations: [BreadcrumbComponent],
    exports: [BreadcrumbComponent]
})

export class BreadcrumbModule { }
