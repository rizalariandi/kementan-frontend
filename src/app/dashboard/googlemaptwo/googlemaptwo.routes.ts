import { Route } from '@angular/router';

import { GooglemaptwoComponent } from './index';

export const GooglemaptwoRoutes: Route[] = [
  {
    path: 'googlemaptwo',
    component: GooglemaptwoComponent
  }
];
