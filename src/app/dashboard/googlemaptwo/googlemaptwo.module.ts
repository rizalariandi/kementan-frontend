import { NgModule } from '@angular/core';

import { GooglemaptwoComponent } from './googlemaptwo.component';

@NgModule({
    imports: [],
    declarations: [GooglemaptwoComponent],
    exports: [GooglemaptwoComponent]
})

export class GooglemaptwoModule { }
