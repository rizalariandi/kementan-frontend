import { Route } from '@angular/router';
import { KementanComponent } from './index';

export const KementanRoutes: Route[] = [
    {
      path: 'kementan',
      component: KementanComponent
    }
];
