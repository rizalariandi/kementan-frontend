import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { KementanComponent, LineChartComponent } from './kementan.component';
import { AccordionModule, AlertModule,
    ButtonsModule, CarouselModule, CollapseModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProgressbarModule,
    RatingModule,
    SortableModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';
import {SafePipe} from 'app/pipe/SafePipe';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports: [CommonModule, ChartsModule, 
        AccordionModule,
        FormsModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        PopoverModule,
        ProgressbarModule,
        RatingModule,
        SortableModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule.forRoot()],
    declarations: [KementanComponent, LineChartComponent,SafePipe],
    exports: [KementanComponent, LineChartComponent]
})

export class KementanModule { }
