import { NgModule } from '@angular/core';

import { ErrorfourComponent } from './errorfour.component';

@NgModule({
    imports: [],
    declarations: [ErrorfourComponent],
    exports: [ErrorfourComponent]
})

export class ErrorfourModule { }
