import { Route } from '@angular/router';

import { ErrorfourComponent } from './index';

export const ErrorfourRoutes: Route[] = [
  {
    path: 'errorfour',
    component: ErrorfourComponent
  }
];
