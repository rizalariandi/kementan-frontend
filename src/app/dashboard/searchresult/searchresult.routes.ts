import { Route } from '@angular/router';

import { SearchresultComponent } from './index';

export const SearchresultRoutes: Route[] = [
  {
    path: 'searchresult',
    component: SearchresultComponent
  }
];
