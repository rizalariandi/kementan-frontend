import { NgModule } from '@angular/core';

import { SearchresultComponent } from './searchresult.component';

@NgModule({
    imports: [],
    declarations: [SearchresultComponent],
    exports: [SearchresultComponent]
})

export class SearchresultModule { }
