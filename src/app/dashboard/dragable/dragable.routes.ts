import { Route } from '@angular/router';

import { DragableComponent } from './index';

export const DragableRoutes: Route[] = [
  {
    path: 'dragable',
    component: DragableComponent
  }
];
