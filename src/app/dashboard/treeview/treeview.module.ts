import { NgModule } from '@angular/core';

import { TreeviewComponent } from './treeview.component';

@NgModule({
    imports: [],
    declarations: [TreeviewComponent],
    exports: [TreeviewComponent]
})

export class TreeviewModule { }
