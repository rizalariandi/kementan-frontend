import { Route } from '@angular/router';

import { TreeviewComponent } from './index';

export const TreeviewRoutes: Route[] = [
  {
    path: 'treeview',
    component: TreeviewComponent
  }
];
