import { NgModule } from '@angular/core';

import { TimelineComponent } from './timeline.component';

@NgModule({
    imports: [],
    declarations: [TimelineComponent],
    exports: [TimelineComponent]
})

export class TimelineModule { }
