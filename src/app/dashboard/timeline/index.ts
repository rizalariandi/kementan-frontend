/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from './timeline.component';
export * from './timeline.routes';

