import { Route } from '@angular/router';

import { AlertsandnotesComponent } from './index';

export const AlertsandnotesRoutes: Route[] = [
  {
    path: 'alertsnotes',
    component: AlertsandnotesComponent
  }
];
