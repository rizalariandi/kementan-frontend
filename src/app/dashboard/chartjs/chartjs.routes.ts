import { Route } from '@angular/router';

import { ChartjsComponent } from './index';

export const ChartjsRoutes: Route[] = [
  {
    path: 'chartjs',
    component: ChartjsComponent
  }
];
