import { NgModule } from '@angular/core';

import { ProductdetailsComponent } from './productdetails.component';

@NgModule({
    imports: [],
    declarations: [ProductdetailsComponent],
    exports: [ProductdetailsComponent]
})

export class ProductdetailsModule { }
