import { Route } from '@angular/router';

import { ProductdetailsComponent } from './index';

export const ProductdetailsRoutes: Route[] = [
  {
    path: 'productdetails',
    component: ProductdetailsComponent
  }
];
