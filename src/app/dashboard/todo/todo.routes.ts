import { Route } from '@angular/router';
import { TodoComponent } from './index';

export const TodoRoutes: Route[] = [
    {
      path: 'todo',
      component: TodoComponent
    }
];
