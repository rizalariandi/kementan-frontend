import { Route } from '@angular/router';

import { BlogComponent } from './index';

export const BlogRoutes: Route[] = [
  {
    path: 'blog',
    component: BlogComponent
  }
];
