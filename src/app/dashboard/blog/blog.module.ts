import { NgModule } from '@angular/core';

import { BlogComponent } from './blog.component';

@NgModule({
    imports: [],
    declarations: [BlogComponent],
    exports: [BlogComponent]
})

export class BlogModule { }
