import { Route } from '@angular/router';

import { ClipboardComponent } from './index';

export const ClipboardRoutes: Route[] = [
  {
    path: 'clipboard',
    component: ClipboardComponent
  }
];
