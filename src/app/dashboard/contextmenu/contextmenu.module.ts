import { NgModule } from '@angular/core';

import { ContextmenuComponent } from './contextmenu.component';

@NgModule({
    imports: [],
    declarations: [ContextmenuComponent],
    exports: [ContextmenuComponent]
})

export class ContextmenuModule { }
