import { Route } from '@angular/router';

import { ContextmenuComponent } from './index';

export const ContextmenuRoutes: Route[] = [
  {
    path: 'contextmenu',
    component: ContextmenuComponent
  }
];
