import { Route } from '@angular/router';

import { AutocompleteComponent } from './index';

export const AutocompleteRoutes: Route[] = [
  {
    path: 'autocomplete',
    component: AutocompleteComponent
  }
];
