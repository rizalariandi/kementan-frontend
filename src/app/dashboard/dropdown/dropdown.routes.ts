import { Route } from '@angular/router';

import { DropdownComponent } from './index';

export const DropdownRoutes: Route[] = [
  {
    path: 'Dropdown',
    component: DropdownComponent
  }
];
