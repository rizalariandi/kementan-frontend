import { NgModule } from '@angular/core';

import { DropdownComponent } from './dropdown.component';

@NgModule({
    imports: [],
    declarations: [DropdownComponent],
    exports: [DropdownComponent]
})

export class DropdownModule { }
