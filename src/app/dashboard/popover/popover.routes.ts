import { Route } from '@angular/router';

import { PopoverComponent } from './index';

export const PopoverRoutes: Route[] = [
  {
    path: 'popover',
    component: PopoverComponent
  }
];
