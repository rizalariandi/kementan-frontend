import { Route } from '@angular/router';

import { InvoiceoneComponent } from './index';

export const InvoiceoneRoutes: Route[] = [
  {
    path: 'invoice1',
    component: InvoiceoneComponent
  }
];
