import { NgModule } from '@angular/core';

import { InvoiceoneComponent } from './invoiceone.component';

@NgModule({
    imports: [],
    declarations: [InvoiceoneComponent],
    exports: [InvoiceoneComponent]
})

export class InvoiceoneModule { }
