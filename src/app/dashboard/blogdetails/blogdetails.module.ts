import { NgModule } from '@angular/core';

import { BlogdetailsComponent } from './blogdetails.component';

@NgModule({
    imports: [],
    declarations: [BlogdetailsComponent],
    exports: [BlogdetailsComponent]
})

export class BlogdetailsModule { }
