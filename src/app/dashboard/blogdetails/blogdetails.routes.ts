import { Route } from '@angular/router';

import { BlogdetailsComponent } from './index';

export const BlogdetailsRoutes: Route[] = [
  {
    path: 'blogdetails',
    component: BlogdetailsComponent
  }
];
