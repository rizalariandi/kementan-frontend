import { Route } from '@angular/router';

import { PricingComponent } from './index';

export const PricingRoutes: Route[] = [
  {
    path: 'pricing',
    component: PricingComponent
  }
];
