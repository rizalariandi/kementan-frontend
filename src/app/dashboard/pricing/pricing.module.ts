import { NgModule } from '@angular/core';

import { PricingComponent } from './pricing.component';

@NgModule({
    imports: [],
    declarations: [PricingComponent],
    exports: [PricingComponent]
})

export class PricingModule { }
