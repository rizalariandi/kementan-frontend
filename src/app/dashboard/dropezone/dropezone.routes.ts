import { Route } from '@angular/router';

import { DropezoneComponent } from './index';

export const DropezoneRoutes: Route[] = [
  {
    path: 'dropezone',
    component: DropezoneComponent
  }
];
