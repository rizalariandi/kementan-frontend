import { Route } from '@angular/router';

import { SocialprofileComponent } from './index';

export const SocialprofileRoutes: Route[] = [
  {
    path: 'socialprofile',
    component: SocialprofileComponent
  }
];
