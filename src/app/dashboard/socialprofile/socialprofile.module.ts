import { NgModule } from '@angular/core';

import { SocialprofileComponent } from './socialprofile.component';

@NgModule({
    imports: [],
    declarations: [SocialprofileComponent],
    exports: [SocialprofileComponent]
})

export class SocialprofileModule { }
