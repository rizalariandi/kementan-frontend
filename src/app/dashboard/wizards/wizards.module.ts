import { NgModule } from '@angular/core';

import { WizardsComponent } from './wizards.component';

@NgModule({
    imports: [],
    declarations: [WizardsComponent],
    exports: [WizardsComponent]
})

export class WizardsModule { }
