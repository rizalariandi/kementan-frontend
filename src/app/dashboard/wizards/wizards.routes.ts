import { Route } from '@angular/router';

import { WizardsComponent } from './index';

export const WizardsRoutes: Route[] = [
  {
    path: 'wizards',
    component: WizardsComponent
  }
];
