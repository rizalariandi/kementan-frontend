import { Route } from '@angular/router';
import { NetworkingComponent } from './index';

export const NetworkingRoutes: Route[] = [
    {
      path: 'networking',
      component: NetworkingComponent
    }
];
