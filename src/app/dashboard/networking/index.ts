/**
*  This barrel file provides the export for the lazy loaded HomeComponent.
*/
export * from './networking.component';
export * from './networking.routes';
