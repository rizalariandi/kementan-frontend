import { NgModule } from '@angular/core';

import { FootableComponent } from './footable.component';

@NgModule({
    imports: [],
    declarations: [FootableComponent],
    exports: [FootableComponent]
})

export class FootableModule { }
