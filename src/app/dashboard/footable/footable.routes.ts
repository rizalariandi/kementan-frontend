import { Route } from '@angular/router';

import { FootableComponent } from './index';

export const FootableRoutes: Route[] = [
  {
    path: 'footable',
    component: FootableComponent
  }
];
