import { Route } from '@angular/router';
import { LtrComponent } from './index';

export const LtrRoutes: Route[] = [
    {
      path: 'ltr',
      component: LtrComponent
    }
];
