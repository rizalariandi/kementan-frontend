/**
*  This barrel file provides the export for the lazy loaded HomeComponent.
*/
export * from './ltr.component';
export * from './ltr.routes';
