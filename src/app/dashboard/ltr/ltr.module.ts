import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LtrComponent } from './ltr.component';
import { AccordionModule, AlertModule,
    ButtonsModule, CarouselModule, CollapseModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProgressbarModule,
    RatingModule,
    SortableModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';

import { TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent } from './ltr.component';

@NgModule({
    imports: [CommonModule, ChartsModule, 
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        PopoverModule,
        ProgressbarModule,
        RatingModule,
        SortableModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule.forRoot()],
    declarations: [LtrComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent],
    exports: [LtrComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent]
})

export class LtrModule { }
