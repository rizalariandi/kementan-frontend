import { Route } from '@angular/router';

import { CkeditorComponent } from './index';

export const CkeditorRoutes: Route[] = [
  {
    path: 'ckeditor',
    component: CkeditorComponent
  }
];
