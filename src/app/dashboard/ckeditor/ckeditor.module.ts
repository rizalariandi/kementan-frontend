import { NgModule } from '@angular/core';

import { CkeditorComponent } from './ckeditor.component';

@NgModule({
    imports: [],
    declarations: [CkeditorComponent],
    exports: [CkeditorComponent]
})

export class CkeditorModule { }
