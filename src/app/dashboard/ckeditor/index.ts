/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from './ckeditor.component';
export * from './ckeditor.routes';

