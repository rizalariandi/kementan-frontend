import { Route } from '@angular/router';

import { SwitchComponent } from './index';

export const SwitchRoutes: Route[] = [
  {
    path: 'switch',
    component: SwitchComponent
  }
];
