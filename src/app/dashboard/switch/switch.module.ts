import { NgModule } from '@angular/core';

import { SwitchComponent } from './switch.component';

@NgModule({
    imports: [],
    declarations: [SwitchComponent],
    exports: [SwitchComponent]
})

export class SwitchModule { }
