import { Route } from '@angular/router';

import { EmployeereviewformComponent } from './index';

export const EmployeereviewformRoutes: Route[] = [
  {
    path: 'employeereviewform',
    component: EmployeereviewformComponent
  }
];
