import { NgModule } from '@angular/core';

import { EmployeereviewformComponent } from './employeereviewform.component';

@NgModule({
    imports: [],
    declarations: [EmployeereviewformComponent],
    exports: [EmployeereviewformComponent]
})

export class EmployeereviewformModule { }
