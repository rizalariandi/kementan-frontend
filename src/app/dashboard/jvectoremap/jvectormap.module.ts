import { NgModule } from '@angular/core';

import { JvectormapComponent } from './jvectormap.component';

@NgModule({
    imports: [],
    declarations: [JvectormapComponent],
    exports: [JvectormapComponent]
})

export class JvectormapModule { }
