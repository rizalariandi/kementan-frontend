import { Route } from '@angular/router';

import { JvectormapComponent } from './index';

export const JvectormapRoutes: Route[] = [
  {
    path: 'jvectormap',
    component: JvectormapComponent
  }
];
