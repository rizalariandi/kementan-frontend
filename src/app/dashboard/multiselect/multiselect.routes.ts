import { Route } from '@angular/router';

import { MultiselectComponent } from './index';

export const MultiselectRoutes: Route[] = [
  {
    path: 'multiselect',
    component: MultiselectComponent
  }
];
