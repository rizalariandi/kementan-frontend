import { NgModule } from '@angular/core';


import { MultiselectComponent } from './multiselect.component';

@NgModule({
    imports: [],
    declarations: [MultiselectComponent],
    exports: [MultiselectComponent]
})

export class MultiselectModule { }
