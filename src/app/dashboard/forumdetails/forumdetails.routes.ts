import { Route } from '@angular/router';

import { ForumdetailsComponent } from './index';

export const ForumdetailsRoutes: Route[] = [
  {
    path: 'forumdetails',
    component: ForumdetailsComponent
  }
];
