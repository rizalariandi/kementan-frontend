import { NgModule } from '@angular/core';

import { ForumdetailsComponent } from './forumdetails.component';

@NgModule({
    imports: [],
    declarations: [ForumdetailsComponent],
    exports: [ForumdetailsComponent]
})

export class ForumdetailsModule { }
