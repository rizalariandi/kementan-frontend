import { Route } from '@angular/router';

import { ButtonsComponent } from './index';

export const ButtonsRoutes: Route[] = [
  {
    path: 'buttons',
    component: ButtonsComponent
  }
];
