import { Route } from '@angular/router';

import { HighchartComponent } from './index';

export const HighchartRoutes: Route[] = [
  {
    path: 'highchart',
    component: HighchartComponent
  }
];
