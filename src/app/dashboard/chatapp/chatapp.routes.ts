import { Route } from '@angular/router';

import { ChatappComponent } from './index';

export const ChatappRoutes: Route[] = [
  {
    path: 'chatapp',
    component: ChatappComponent
  }
];
