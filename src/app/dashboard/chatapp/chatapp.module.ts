import { NgModule } from '@angular/core';

import { ChatappComponent } from './chatapp.component';

@NgModule({
    imports: [],
    declarations: [ChatappComponent],
    exports: [ChatappComponent]
})

export class ChatappModule { }
