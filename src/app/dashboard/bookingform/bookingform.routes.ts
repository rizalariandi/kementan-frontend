import { Route } from '@angular/router';

import { BookingformComponent } from './index';

export const BookingformRoutes: Route[] = [
  {
    path: 'bookingform',
    component: BookingformComponent
  }
];
