import { NgModule } from '@angular/core';

import { BookingformComponent } from './bookingform.component';

@NgModule({
    imports: [],
    declarations: [BookingformComponent],
    exports: [BookingformComponent]
})

export class BookingformModule { }
