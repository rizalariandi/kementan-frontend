import { Route } from '@angular/router';

import { ModalComponent } from './index';

export const ModalRoutes: Route[] = [
  {
    path: 'modal',
    component: ModalComponent
  }
];
