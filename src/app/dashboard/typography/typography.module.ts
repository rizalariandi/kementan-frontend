import { NgModule } from '@angular/core';

import { TypographyComponent } from './typography.component';

@NgModule({
    imports: [],
    declarations: [TypographyComponent],
    exports: [TypographyComponent]
})

export class TypographyModule { }
