/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from './typography.component';
export * from './typography.routes';

