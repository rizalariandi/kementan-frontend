import { Route } from '@angular/router';

import { TypographyComponent } from './index';

export const TypographyRoutes: Route[] = [
  {
    path: 'typography',
    component: TypographyComponent
  }
];
