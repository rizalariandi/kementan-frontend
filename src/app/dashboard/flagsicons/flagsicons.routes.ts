import { Route } from '@angular/router';

import { FlagsiconsComponent } from './index';

export const FlagsiconsRoutes: Route[] = [
  {
    path: 'flagsicons',
    component: FlagsiconsComponent
  }
];
