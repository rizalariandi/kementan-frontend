import { NgModule } from '@angular/core';

import { FlagsiconsComponent } from './flagsicons.component';

@NgModule({
    imports: [],
    declarations: [FlagsiconsComponent],
    exports: [FlagsiconsComponent]
})

export class FlagsiconsModule { }
