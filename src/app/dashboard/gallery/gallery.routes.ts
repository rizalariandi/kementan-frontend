import { Route } from '@angular/router';

import { GalleryComponent } from './index';

export const GalleryRoutes: Route[] = [
  {
    path: 'gallery',
    component: GalleryComponent
  }
];
