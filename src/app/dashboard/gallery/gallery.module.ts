import { NgModule } from '@angular/core';

import { GalleryComponent } from './gallery.component';

@NgModule({
    imports: [],
    declarations: [GalleryComponent],
    exports: [GalleryComponent]
})

export class GalleryModule { }
