import { Route } from '@angular/router';

import { RangesliderComponent } from './index';

export const RangesliderRoutes: Route[] = [
  {
    path: 'rangeslider',
    component: RangesliderComponent
  }
];
