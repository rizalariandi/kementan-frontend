import { NgModule } from '@angular/core';

import { RangesliderComponent } from './rangeslider.component';

@NgModule({
    imports: [],
    declarations: [RangesliderComponent],
    exports: [RangesliderComponent]
})

export class RangesliderModule { }
