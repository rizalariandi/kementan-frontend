import { NgModule } from '@angular/core';

import { DonationformComponent } from './donationform.component';

@NgModule({
    imports: [],
    declarations: [DonationformComponent],
    exports: [DonationformComponent]
})

export class DonationformModule { }
