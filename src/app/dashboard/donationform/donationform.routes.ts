import { Route } from '@angular/router';

import { DonationformComponent } from './index';

export const DonationformRoutes: Route[] = [
  {
    path: 'donationform',
    component: DonationformComponent
  }
];
