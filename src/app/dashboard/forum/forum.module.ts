import { NgModule } from '@angular/core';

import { ForumComponent } from './forum.component';

@NgModule({
    imports: [],
    declarations: [ForumComponent],
    exports: [ForumComponent]
})

export class ForumModule { }
