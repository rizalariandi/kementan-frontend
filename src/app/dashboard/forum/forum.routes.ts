import { Route } from '@angular/router';

import { ForumComponent } from './index';

export const ForumRoutes: Route[] = [
  {
    path: 'forum',
    component: ForumComponent
  }
];
