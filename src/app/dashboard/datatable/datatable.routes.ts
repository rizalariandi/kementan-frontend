import { Route } from '@angular/router';

import { DatatableComponent } from './index';

export const DatatableRoutes: Route[] = [
  {
    path: 'datatable',
    component: DatatableComponent
  }
];
