import { NgModule } from '@angular/core';

import { DatatableComponent } from './datatable.component';

@NgModule({
    imports: [],
    declarations: [DatatableComponent],
    exports: [DatatableComponent]
})

export class DatatableModule { }
