import { NgModule } from '@angular/core';

import { CreditcardtwoComponent } from './creditcardtwo.component';

@NgModule({
    imports: [],
    declarations: [CreditcardtwoComponent],
    exports: [CreditcardtwoComponent]
})

export class CreditcardtwoModule { }
