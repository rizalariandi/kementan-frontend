import { Route } from '@angular/router';

import { CreditcardtwoComponent } from './index';

export const CreditcardtwoRoutes: Route[] = [
  {
    path: 'creditcardtwo',
    component: CreditcardtwoComponent
  }
];
