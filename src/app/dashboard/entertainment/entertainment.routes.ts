import { Route } from '@angular/router';
import { EntertainmentComponent } from './index';

export const EntertainmentRoutes: Route[] = [
    {
      path: 'entertainment',
      component: EntertainmentComponent
    }
];
