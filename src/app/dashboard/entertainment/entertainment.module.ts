import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EntertainmentComponent } from './entertainment.component';
import { AccordionModule, AlertModule,
    ButtonsModule, CarouselModule, CollapseModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProgressbarModule,
    RatingModule,
    SortableModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';


@NgModule({
    imports: [CommonModule, ChartsModule, 
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        PopoverModule,
        ProgressbarModule,
        RatingModule,
        SortableModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule.forRoot()],
    declarations: [EntertainmentComponent],
    exports: [EntertainmentComponent]
})

export class EntertainmentModule { }
