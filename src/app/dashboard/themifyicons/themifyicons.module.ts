import { NgModule } from '@angular/core';

import { ThemifyiconsComponent } from './themifyicons.component';

@NgModule({
    imports: [],
    declarations: [ThemifyiconsComponent],
    exports: [ThemifyiconsComponent]
})

export class ThemifyiconsModule { }
