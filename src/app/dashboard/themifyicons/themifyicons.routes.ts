import { Route } from '@angular/router';

import { ThemifyiconsComponent } from './index';

export const ThemifyiconsRoutes: Route[] = [
  {
    path: 'themefyicons',
    component: ThemifyiconsComponent
  }
];
