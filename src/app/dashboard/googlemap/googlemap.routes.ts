import { Route } from '@angular/router';

import { GooglemapComponent } from './index';

export const GooglemapRoutes: Route[] = [
  {
    path: 'googlemap',
    component: GooglemapComponent
  }
];
