import { NgModule } from '@angular/core';

import { GooglemapComponent } from './googlemap.component';

@NgModule({
    imports: [],
    declarations: [GooglemapComponent],
    exports: [GooglemapComponent]
})

export class GooglemapModule { }
