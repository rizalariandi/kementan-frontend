import { NgModule } from '@angular/core';

import { UserlistComponent } from './userlist.component';

@NgModule({
    imports: [],
    declarations: [UserlistComponent],
    exports: [UserlistComponent]
})

export class UserlistModule { }
