import { Route } from '@angular/router';

import { UserlistComponent } from './index';

export const UserlistRoutes: Route[] = [
  {
    path: 'userlist',
    component: UserlistComponent
  }
];
