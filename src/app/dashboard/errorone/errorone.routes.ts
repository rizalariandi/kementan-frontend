import { Route } from '@angular/router';

import { ErroroneComponent } from './index';

export const ErroroneRoutes: Route[] = [
  {
    path: 'errorone',
    component: ErroroneComponent
  }
];
