import { NgModule } from '@angular/core';

import { ErroroneComponent } from './errorone.component';

@NgModule({
    imports: [],
    declarations: [ErroroneComponent],
    exports: [ErroroneComponent]
})

export class ErroroneModule { }
