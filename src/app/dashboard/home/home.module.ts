import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import { AccordionModule, AlertModule,
    ButtonsModule, CarouselModule, CollapseModule,
    BsDropdownModule,
    ModalModule,
    PaginationModule,
    PopoverModule,
    ProgressbarModule,
    RatingModule,
    SortableModule,
    TabsModule,
    TimepickerModule,
    TooltipModule,
    TypeaheadModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts';

import { TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent } from './home.component';

@NgModule({
    imports: [CommonModule, ChartsModule,
        AccordionModule,
        AlertModule,
        ButtonsModule,
        CarouselModule,
        CollapseModule,
        BsDropdownModule,
        ModalModule,
        PaginationModule,
        PopoverModule,
        ProgressbarModule,
        RatingModule,
        SortableModule,
        TabsModule,
        TimepickerModule,
        TooltipModule,
        TypeaheadModule.forRoot()],
    declarations: [HomeComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent],
    exports: [HomeComponent, TimelineComponent, ChatComponent, NotificationComponent, LineChartComponent]
})

export class HomeModule { }
