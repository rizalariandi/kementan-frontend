import { NgModule } from '@angular/core';

import { CookieComponent } from './cookie.component';

@NgModule({
    imports: [],
    declarations: [CookieComponent],
    exports: [CookieComponent]
})

export class CookieModule { }
