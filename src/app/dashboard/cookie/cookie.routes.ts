import { Route } from '@angular/router';

import { CookieComponent } from './index';

export const CookieRoutes: Route[] = [
  {
    path: 'cookie',
    component: CookieComponent
  }
];
