import { Route } from '@angular/router';

import { ProductsComponent } from './index';

export const ProductsRoutes: Route[] = [
  {
    path: 'product',
    component: ProductsComponent
  }
];
