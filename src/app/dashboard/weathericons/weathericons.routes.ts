import { Route } from '@angular/router';

import { WeathericonsComponent } from './index';

export const WeathericonsRoutes: Route[] = [
  {
    path: 'weathericons',
    component: WeathericonsComponent
  }
];
