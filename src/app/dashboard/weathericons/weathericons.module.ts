import { NgModule } from '@angular/core';

import { WeathericonsComponent } from './weathericons.component';

@NgModule({
    imports: [],
    declarations: [WeathericonsComponent],
    exports: [WeathericonsComponent]
})

export class WeathericonsModule { }
