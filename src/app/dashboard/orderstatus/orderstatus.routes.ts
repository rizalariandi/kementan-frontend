import { Route } from '@angular/router';

import { OrderstatusComponent } from './index';

export const OrderstatusRoutes: Route[] = [
  {
    path: 'orderstatus',
    component: OrderstatusComponent
  }
];
