import { NgModule } from '@angular/core';

import { OrderstatusComponent } from './orderstatus.component';

@NgModule({
    imports: [],
    declarations: [OrderstatusComponent],
    exports: [OrderstatusComponent]
})

export class OrderstatusModule { }
