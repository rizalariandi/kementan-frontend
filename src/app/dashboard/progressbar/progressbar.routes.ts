import { Route } from '@angular/router';

import { ProgressbarComponent } from './index';

export const ProgressbarRoutes: Route[] = [
  {
    path: 'progressbar',
    component: ProgressbarComponent
  }
];
