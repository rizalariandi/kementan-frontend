import { NgModule } from '@angular/core';

import { ProgressbarComponent } from './progressbar.component';

@NgModule({
    imports: [],
    declarations: [ProgressbarComponent],
    exports: [ProgressbarComponent]
})

export class ProgressbarModules { }
