import { Route } from '@angular/router';

import { CalendarComponent } from './index';

export const CalendarRoutes: Route[] = [
  {
    path: 'calendar',
    component: CalendarComponent
  }
];
