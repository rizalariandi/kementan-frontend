import { NgModule } from '@angular/core';

import { CalendarComponent } from './calendar.component';

@NgModule({
    imports: [],
    declarations: [CalendarComponent],
    exports: [CalendarComponent]
})

export class CalendarModule { }
