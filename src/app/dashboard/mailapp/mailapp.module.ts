import { NgModule } from '@angular/core';

import { MailappComponent } from './mailapp.component';

@NgModule({
    imports: [],
    declarations: [MailappComponent],
    exports: [MailappComponent]
})

export class MailappModule { }
