/**
 * This barrel file provides the export for the lazy loaded BlankpageComponent.
 */
export * from './mailapp.component';
export * from './mailapp.routes';

