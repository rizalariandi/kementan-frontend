import { Route } from '@angular/router';

import { MailappComponent } from './index';

export const MailappRoutes: Route[] = [
  {
    path: 'mailbox',
    component: MailappComponent
  }
];
