import { NgModule } from '@angular/core';

import { ErrorthreeComponent } from './errorthree.component';

@NgModule({
    imports: [],
    declarations: [ErrorthreeComponent],
    exports: [ErrorthreeComponent]
})

export class ErrorthreeModule { }
