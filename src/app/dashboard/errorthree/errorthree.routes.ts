import { Route } from '@angular/router';

import { ErrorthreeComponent } from './index';

export const ErrorthreeRoutes: Route[] = [
  {
    path: 'errorthree',
    component: ErrorthreeComponent
  }
];
