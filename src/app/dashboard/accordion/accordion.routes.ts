import { Route } from '@angular/router';

import { AccordionComponent } from './accordion.component';

export const AccordionRoutes: Route[] = [
  {
    path: 'accordion',
    component: AccordionComponent
  }
];
