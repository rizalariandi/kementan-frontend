import { NgModule } from '@angular/core';

import { FontawesomeiconComponent } from './fontawesomeicon.component';

@NgModule({
    imports: [],
    declarations: [FontawesomeiconComponent],
    exports: [FontawesomeiconComponent]
})

export class FontawesomeiconModule { }
