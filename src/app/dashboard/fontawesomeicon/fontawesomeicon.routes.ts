import { Route } from '@angular/router';

import { FontawesomeiconComponent } from './index';

export const FontawesomeiconRoutes: Route[] = [
  {
    path: 'fontawesomeicons',
    component: FontawesomeiconComponent
  }
];
