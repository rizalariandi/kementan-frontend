import { Route } from '@angular/router';

import { ProfileformComponent } from './index';

export const ProfileformRoutes: Route[] = [
  {
    path: 'profileform',
    component: ProfileformComponent
  }
];
