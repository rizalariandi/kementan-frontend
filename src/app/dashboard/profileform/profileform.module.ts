import { NgModule } from '@angular/core';

import { ProfileformComponent } from './profileform.component';

@NgModule({
    imports: [],
    declarations: [ProfileformComponent],
    exports: [ProfileformComponent]
})

export class ProfileformModule { }
