import { Route } from '@angular/router';
import { Signup3Component } from './signup3.component';

export const Signup3Routes: Route[] = [
    {
      path: 'signup3',
      component: Signup3Component
    }
];
