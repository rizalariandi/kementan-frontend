import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Signup3Component } from './signup3.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Signup3Component],
    exports: [Signup3Component]
})

export class Signup3Module { }
