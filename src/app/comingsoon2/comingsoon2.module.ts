import { NgModule } from '@angular/core';

import { Comingsoon2Component } from './comingsoon2.component';

@NgModule({
    imports: [],
    declarations: [Comingsoon2Component],
    exports: [Comingsoon2Component]
})

export class Comingsoon2Module { }
