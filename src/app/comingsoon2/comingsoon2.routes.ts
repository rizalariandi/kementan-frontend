import { Route } from '@angular/router';

import { Comingsoon2Component } from './comingsoon2.component';

export const Comingsoon2Routes: Route[] = [
  {
    path: 'comingsoon2',
    component: Comingsoon2Component
  }
];
