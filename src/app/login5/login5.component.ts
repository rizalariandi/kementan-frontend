import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded LoginComponent.
*/

@Component({
  selector: 'app-login-cmp',
  templateUrl: 'login5.component.html'
})

export class Login5Component { }
