import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { Login5Component } from './login5.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Login5Component],
    exports: [Login5Component]
})

export class Login5Module { }
