import { Routes } from '@angular/router';
import { Login5Component } from './login5.component';

export const Login5Routes: Routes = [
    {
      path: 'signin5', component: Login5Component
    }
];
