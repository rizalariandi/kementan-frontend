import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Resetpassword2Component } from './resetpassword2.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Resetpassword2Component],
    exports: [Resetpassword2Component]
})

export class Resetpassword2Module { }
