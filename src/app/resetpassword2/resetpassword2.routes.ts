import { Route } from '@angular/router';
import { Resetpassword2Component } from './resetpassword2.component';

export const Resetpassword2Routes: Route[] = [
    {
      path: 'resetpassword2',
      component: Resetpassword2Component
    }
];
