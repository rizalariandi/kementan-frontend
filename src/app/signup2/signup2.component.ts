import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded SignupComponent.
*/

@Component({
  selector: 'app-signup-cmp',
  templateUrl: 'signup2.component.html'
})

export class Signup2Component { }
