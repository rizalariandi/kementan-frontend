import { Route } from '@angular/router';
import { Signup2Component } from './signup2.component';

export const Signup2Routes: Route[] = [
    {
      path: 'signup2',
      component: Signup2Component
    }
];
