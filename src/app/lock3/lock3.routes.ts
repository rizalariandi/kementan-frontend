import { Route } from '@angular/router';
import { Lock3Component } from './lock3.component';

export const Lock3Routes: Route[] = [
    {
      path: 'lock3',
      component: Lock3Component
    }
];
