import { NgModule } from '@angular/core';

import { Comingsoon3Component } from './comingsoon3.component';

@NgModule({
    imports: [],
    declarations: [Comingsoon3Component],
    exports: [Comingsoon3Component]
})

export class Comingsoon3Module { }
