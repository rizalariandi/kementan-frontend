import { Route } from '@angular/router';

import { Comingsoon3Component } from './comingsoon3.component';

export const Comingsoon3Routes: Route[] = [
  {
    path: 'comingsoon3',
    component: Comingsoon3Component
  }
];
