import { Route } from '@angular/router';
import { Lock6Component } from './lock6.component';

export const Lock6Routes: Route[] = [
    {
      path: 'lock6',
      component: Lock6Component
    }
];
