import { Route } from '@angular/router';
import { Resetpassword3Component } from './resetpassword3.component';

export const Resetpassword3Routes: Route[] = [
    {
      path: 'resetpassword3',
      component: Resetpassword3Component
    }
];
