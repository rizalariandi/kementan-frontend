import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Resetpassword3Component } from './resetpassword3.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Resetpassword3Component],
    exports: [Resetpassword3Component]
})

export class Resetpassword3Module { }
