import { Route } from '@angular/router';
import { Lock2Component } from './lock2.component';

export const Lock2Routes: Route[] = [
    {
      path: 'lock2',
      component: Lock2Component
    }
];
