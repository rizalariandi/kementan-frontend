import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded SignupComponent.
*/

@Component({
  selector: 'app-signup-cmp',
  templateUrl: 'lock2.component.html'
})

export class Lock2Component { }
