import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

import { Resetpassword4Component } from './resetpassword4.component';

@NgModule({
    imports: [CommonModule, RouterModule],
    declarations: [Resetpassword4Component],
    exports: [Resetpassword4Component]
})

export class Resetpassword4Module { }
