import { Route } from '@angular/router';
import { Resetpassword4Component } from './resetpassword4.component';

export const Resetpassword4Routes: Route[] = [
    {
      path: 'resetpassword4',
      component: Resetpassword4Component
    }
];
