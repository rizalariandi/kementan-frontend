import { Component } from '@angular/core';

/**
*  This class represents the lazy loaded SignupComponent.
*/

@Component({
  selector: 'app-signup-cmp',
  templateUrl: 'resetpassword4.component.html'
})

export class Resetpassword4Component { }
