import { Route } from '@angular/router';
import { Lock5Component } from './lock5.component';

export const Lock5Routes: Route[] = [
    {
      path: 'lock5',
      component: Lock5Component
    }
];
