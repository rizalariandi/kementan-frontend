import { Route } from '@angular/router';

import { Comingsoon1Component } from './comingsoon1.component';

export const Comingsoon1Routes: Route[] = [
  {
    path: 'comingsoon1',
    component: Comingsoon1Component
  }
];
