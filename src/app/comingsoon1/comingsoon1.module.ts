import { NgModule } from '@angular/core';

import { Comingsoon1Component } from './comingsoon1.component';

@NgModule({
    imports: [],
    declarations: [Comingsoon1Component],
    exports: [Comingsoon1Component]
})

export class Comingsoon1Module { }
